#include <stdio.h>
int main(){
    int b=0;
    int l=0;
    int a=0;

    while((b%2 == 0) || (b < 3)) {
        printf("Digite a largura da base da arvore.\n");
        printf("*Deve ser impar e maior que 3\n");
        scanf("%d", &b);
        if((b%2 == 0) && (b <= 3)) {
            printf("Valor invalido\n");
        }
    }

    // Enquanto l par ou maior que a metade de b
    while((l%2 ==0) || (l > b/2)){
        printf("Digite a largura do tronco da arvore\n");
        printf("*Deve ser impar, maior que 1 e menor ou igual a %d \n", b/2);
        scanf("%d", &l);
        if((l%2 ==0) && (l >= b/2)){
            printf("Valor invalido\n");
        }
    }

    // Enquanto a menor que 2 ou maior que a metade de b
    while((a < 2) || ( a> b/2)){
        printf("Digite a altura do tronco da arvore\n");
        printf("Deve ser maior que 2 e menor ou igual a %d\n", b/2);
        scanf("%d", &a);
        if((a < 2) && ( a>= b/2)){
            printf("Valor invalido\n");
        }
    }
    // imprimir a parte superior
    int i;
    int j;
    int k;
    int margin = b/2;
    int print = 1;
    while(margin >=0 ){
        for(i=0; i< margin; i++){
            printf(" ");
        }

        for(j=0; j< print; j++){
            printf("*");
        }
        for(k=0; k< margin; k++){
            printf(" ");
        }

        printf("\n");
        margin -= 1;
        print += 2;
        }


    // imprimir o tronco
    // imprimir a margen inicial
    int tmargin = (b-l)/2;
    for(i=0;i<a;i++){
        for(j=0;j <tmargin;j++){
        printf(" ");
        }

        for(k=0;k<l;k++){
            printf("*");
        }
        printf("\n");
    }

    return 0;
}
