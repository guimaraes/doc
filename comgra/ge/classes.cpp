#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>

//Classes
class Player {

    public:
        int position = 0;
        int lives=3;
        int score=0;
        int hits=0;
        float speed=1;
        int delay = 1500;

                
        void Movement(int value){
            if(lives>0){
                if((position+value >= -30) && (position + value <= 30)){
                    position += value;
                }
            }
        }
        void SetPosition(int pos){
            if(lives >0){
                position = pos;
            }
        }
        void GameOver(void){
            if(lives == 0){
                speed = 0.0;
                printf("gameover\n");
                //end msg here
            }
        }

        void Reset(void){
            lives = 3;
            score = 0;
            hits = 0;
            speed = 1.0;
            delay = 1500;
            position = 0;
        }


        void Progression(void){
            if (hits % 4 == 0){
                if(speed <=6.0){
                    speed += 0.020;
                }
                if(delay >500){
                    delay = delay -100;
                }
                printf("spd %f\n",speed);
                printf("hits %d\n",hits);
                printf("delay %d\n",delay);
                printf("hits %d",hits);

            }
        }

};

class HitObject{
    public:
        int posx;
        float posy = 34.0;
        int active = 0;
        float speed = 1.0;

        HitObject(int x){
            posx = x;
        }

        void Reset(Player& player){
            active=0;
            posy=34;
            speed = player.speed;
        }

        void move(Player& player){
            if(active && player.lives >0){
                //printf("%d\n",player.speed);
                posy=posy-speed;
                Miss(player);
                Hit(player);
            }
        }

        void Hit(Player& player){
            if( (posy <= -22.3) && (posx == player.position) ){
                printf("hit\n");
                player.hits+= 1;
                player.score += 100 * speed;
                printf("score %d\n",player.score);
                player.Progression();
                Reset(player);
            }
        }

        void Miss(Player& player){
            if(posy <= -30){
                printf("miss\n");
                player.lives --;
                player.GameOver();
                Reset(player);
            }
        }


};
