#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>
#include <SOIL/SOIL.h>
#include "classes.cpp"
#include <ctime>
#include <string.h>

GLuint tex;
GLuint hit0_tex;
GLuint hit1_tex;
GLuint hit2_tex;
GLuint hit3_tex;
GLuint hit4_tex;
float hitsize = 2.0;
GLUquadric* object;
float angle = 0;
float zscale = -30.0;
float yscale = -25.0;

float mouse_x = -20.0;
float mouse_y = 0.0;
int lastroll = 0;

//Objects
Player player;
HitObject hit0(-30);
HitObject hit1(-15);
HitObject hit2(0);
HitObject hit3(15);
HitObject hit4(30);
HitObject* hit_obj[5];

//camera
int initial_x;
int initial_y;
int drag = 0;
int xpos = 0;
int ypos = 0;
float zoomlevel = 0.0;
static void LoadTexture(char* filename){
        tex = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y );
        if( 0 == tex )
        { printf( "erro do SOIL: '%s'\n", SOIL_last_result() );
        }

        // seta os parametros para a textura
        glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_DECAL);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // habilita a textura 2d para ser inserida nos objetos
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, tex);

}

void imprime(int x, int y,int z, char *string)
{
    glRasterPos2f(x,y);

    int len = (int) strlen(string);

    for (int i = 0; i < len; i++)    {
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10,string[i]);
    }
}


void draw(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
        glColor3f(0.8,1.0,0.8);
    imprime(300,300,zscale,"testando");

glPushMatrix();

//gluLookAt(0,0,60, 0,0,-30, 0,1,0);

	glPushMatrix();
		
	glPopMatrix();
    //draw hit0
    glPushMatrix();
        glTranslatef(hit0.posx, hit0.posy, zscale);
        glScalef(1.0,1.0,0.5);

        glEnable(GL_TEXTURE_2D);
            gluQuadricDrawStyle(object, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, hit0_tex);
            gluQuadricTexture(object, GL_TRUE);
            gluQuadricNormals(object, GLU_SMOOTH);
            gluSphere(object, hitsize, 32, 32.0);
        glDisable(GL_TEXTURE_2D);

    glPopMatrix();

    //draw hit1
    glPushMatrix();
        glTranslatef(hit1.posx, hit1.posy, zscale);
        glScalef(1.0,1.0,0.5);

        glEnable(GL_TEXTURE_2D);
            gluQuadricDrawStyle(object, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, hit1_tex);
            gluQuadricTexture(object, GL_TRUE);
            gluQuadricNormals(object, GLU_SMOOTH);
            gluSphere(object, hitsize, 32, 32);
        glDisable(GL_TEXTURE_2D);

    glPopMatrix();
        glColor3f(1.0,1.0,1.0);
    //draw hit2
    glPushMatrix();
        glTranslatef(hit2.posx, hit2.posy, zscale);
        glScalef(1.0,1.0,0.5);
        glEnable(GL_TEXTURE_2D);
            gluQuadricDrawStyle(object, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, hit2_tex);
            gluQuadricTexture(object, GL_TRUE);
            gluQuadricNormals(object, GLU_SMOOTH);
            gluSphere(object, hitsize, 32, 32);
        glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    //draw hit3
    glPushMatrix();
        glTranslatef(hit3.posx, hit3.posy, zscale);
        glScalef(1.0,1.0,0.5);

    glEnable(GL_TEXTURE_2D);
        gluQuadricDrawStyle(object, GLU_FILL);
        glBindTexture(GL_TEXTURE_2D, hit3_tex);
        gluQuadricTexture(object, GL_TRUE);
        gluQuadricNormals(object, GLU_SMOOTH);
        gluSphere(object, hitsize, 32, 32);
    glDisable(GL_TEXTURE_2D);

    glPopMatrix();

    //draw hit4
    glPushMatrix();
        glTranslatef(hit4.posx, hit4.posy, zscale);
    glEnable(GL_TEXTURE_2D);
            glScalef(1.0,1.0,0.5);
            gluQuadricDrawStyle(object, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, hit4_tex);
            gluQuadricTexture(object, GL_TRUE);
            gluQuadricNormals(object, GLU_SMOOTH);
            gluSphere(object, hitsize, 32, 32);
    glDisable(GL_TEXTURE_2D);

    glPopMatrix();

    //draw player
    glPushMatrix();
        glTranslatef(player.position, yscale, zscale);
        glRotatef(angle, 0, 1, 30);

        glColor3f(0.5,1.0,0.3);
        glScalef(3.0,1.0,1.0);
        glutSolidCube(4.0);

    glPopMatrix();

glPopMatrix();
    glFlush();
    glutSwapBuffers();

}

void Movement(int value){
    if(xpos + value<=30 && xpos + value >=-30){
        xpos += value;
    }
}


void move(int value){

    glutTimerFunc(32, move, 1);
    hit0.move(player);
    hit1.move(player);
    hit2.move(player);
    hit3.move(player);
    hit4.move(player);
    glutPostRedisplay();
}

void resize(int w, int h)
{
    if (!h)
	h = 1;
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90.0, 1.0*w/h, 0.1, 100.0);
}

void init(void)
{
    srand(time(NULL));
    hit_obj[0] = & hit0;
    hit_obj[1] = & hit1;
    hit_obj[2] = & hit2;
    hit_obj[3] = & hit3;
    hit_obj[4] = & hit4;
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.2,0.2,0.2,1.0);
    //make_tex();
    LoadTexture("1.jpg");
    hit0_tex = tex;
    LoadTexture("2.jpg");
    hit1_tex = tex;
    LoadTexture("3.jpg");
    hit2_tex = tex;
    LoadTexture("4.jpg");
    hit3_tex = tex;
    LoadTexture("5.jpg");
    hit4_tex = tex;
    
    object = gluNewQuadric();


}


void Spawn(int x){
    //cria um array com os objetos não ativos
    glutTimerFunc(player.delay, Spawn, 1);
    int value;
        value = rand() %5;
        if(value == lastroll){
            Spawn(1);
        }
        if(hit_obj[value]->active == 0){
            hit_obj[value]->active = 1;
        }
    lastroll = value;
}
void Spawn2(int z){
    glutTimerFunc(player.delay, Spawn2, 1);
    HitObject* inactive[5];
    int x = 0;
    if(hit0.active == 0){
        inactive[x] = & hit0;
        x++;
    }
    if(hit1.active == 0){
        inactive[x] = & hit1;
        x++;
    }
    if(hit2.active == 0){
        inactive[x] = & hit2;
        x++;
    }
    if(hit3.active == 0){
        inactive[x] = & hit3;
        x++;
    }
    if(hit4.active == 0){
        inactive[x] = & hit4;
        x++;
    }

    int value;
    if(x !=0){
        value = rand() %x;
        inactive[value]->active = 1;
    }
}

    //cria um array com os objetos não ativos

void ResetGame(void){
    player.Reset();
    hit0.Reset(player);
    hit1.Reset(player);
    hit2.Reset(player);
    hit3.Reset(player);
    hit4.Reset(player);
}

void KeyPress (unsigned char key, int x, int y) 
{ 
    switch (key) 
    {       

        case 'Q':
            exit(1);
            break;
        case 'R': 
            ResetGame();
            break;   

        case 'a': 
            player.Movement(-15);
            break;   

        case 'd': 
            player.Movement(15);
            break;   

        case 'w': 
            player.SetPosition(-30);
            break;   

        case 's': 
            player.SetPosition(30);
            break;   

        case '1': 
            player.SetPosition(-30);
            break;   

        case '2': 
            player.SetPosition(-15);
            break;   
        case '3': 
            player.SetPosition(0);
            break;   
        case '4': 
            player.SetPosition(15);
            break;   
        case '5': 
            player.SetPosition(30);
            break;   
    } 
    glutPostRedisplay(); 
} 

void mouse(int button, int state, int x, int y)
{
   // 3 scroll up, 4 scroll down
   if ((button == 3) || (button == 4))
   {
       if (state == GLUT_UP) return; // Disregard redundant GLUT_UP events
       printf("Scroll %s At %d %d\n", (button == 3) ? "Up" : "Down", x, y);
       printf("%d\n",zoomlevel);
       if(button == 3){
           zoomlevel += 0.4;
       }else if(button == 4){
           zoomlevel -= 0.4;
       }
   }else{  // botoes normais
       printf("Scroll %s At %d %d\n", (state == GLUT_DOWN) ? "Up" : "Down", x, y);
       if (state == GLUT_DOWN){
           drag = 1;
	   initial_x = x; 
           initial_y = y;
       }else{
           drag = 0;
       }
   }
}

void Dragger(int x, int y){
    if(drag == 1){
        if (x < initial_x){
            xpos -= x/100;
            initial_x = xpos;
        }else if(xpos > initial_x){
            xpos += x/100;
            initial_x = xpos;
        }
        if (y < initial_y){
            ypos -= y/100;
            initial_y = ypos;
        }else if(y > initial_y){
            ypos += y/100;
            initial_y = ypos;
        }
         glutPostRedisplay();
    }
}

//Print


int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(800, 600);

    glutCreateWindow("Test");

    glutDisplayFunc(draw);
    glutReshapeFunc(resize);
    glutTimerFunc(32, move, 1);
    glutMouseFunc(mouse);
    glutMotionFunc(Dragger);
    glutTimerFunc(player.delay, Spawn2, 1);
    glutMouseFunc(mouse);
    glutKeyboardFunc (KeyPress);

    init();

    glutMainLoop();
}
