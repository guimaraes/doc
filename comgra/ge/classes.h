#include <vector>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>


//Classes
class Player {

    public:
        int lives;
        int score;
        int position;
        float speed;

        void Movement(int value);
        void GameOver(void);
};

class HitObject{
    public:
        int posx;
        int posy;
        HitObject(int x);

        void Spawn(void); //drawfunc ?

        void Reset(void);
        void Hit(Player *player);
        void Miss(Player *player);
};
