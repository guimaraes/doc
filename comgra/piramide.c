#include <stdio.h>

int main(){
    int n=0;

    while(n < 2){
        printf("Digite o tamanho da base da piramide.\n");
        printf("*Deve ser  maior que 2\n");
        scanf("%d", &n);
        if(n < 2) {
            printf("Valor invalido\n");
        }
    }

    int margin = n*2 -1;
    int print=1;

    int i;
    while(margin >= 0){
        for(i=0;i < margin;i++){
            printf(" ");
        }

        for(i=1; i <= print; i++){
            printf("%d ", i);
        }

        for(i = print-1; i >= 1; i--){
            printf("%d ", i);
        }
        printf("\n");
        margin -= 2;
        print ++;

    }
}
