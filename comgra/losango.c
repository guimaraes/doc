#include <stdio.h>

int main(){
    int n=0;

    while(n < 3 || n%2 == 0){
        printf("Digite o tamanho da base do losango.\n");
        printf("*Deve ser impar e maior que 3\n");
        scanf("%d", &n);
        if(n < 3 || n%2 == 0) {
            printf("Valor invalido\n");
        }
    }

    int margin = n/2;
    int print=1;

    int i;
    // primeira metade
    while(margin >= 0){
        for(i=0;i < margin;i++){
            printf(" ");
        }

        for(i=1; i <= print; i++){
            printf("X", i);
        }

        for(i=0;i < margin;i++){
            printf(" ");
        }
        printf("\n");
        margin --;
        print +=2;
    }

    print -=2;
    margin ++;
    while(margin <= n/2){
        margin ++;
        print -=2;
        for(i=0;i < margin;i++){
            printf(" ");
        }

        for(i=1; i <= print; i++){
            printf("X", i);
        }

        for(i=0;i < margin;i++){
            printf(" ");
        }
        printf("\n");
    }
}
