#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>
#include <SOIL/SOIL.h>

GLuint tex;
GLUquadric* sphere;
float angle = 0;
float zoomlevel = -15.0;
float mouse_x = -20.0;
float mouse_y = 0.0;
int initial_x;
float final_x;

static void LoadItens(char* filename){
        tex = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y );
        if( 0 == tex )
        {
            printf( "erro do SOIL: '%s'\n", SOIL_last_result() );
        }

        // seta os parametros para a textura
        glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_DECAL);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // habilita a textura 2d para ser inserida nos objetos
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, tex);

}
void mouse(int button, int state, int x, int y)
{
   int current_x = x;
   // 3 scroll up, 4 scroll down
   if ((button == 3) || (button == 4))
   {
       if (state == GLUT_UP) return; // Disregard redundant GLUT_UP events
       printf("Scroll %s At %d %d\n", (button == 3) ? "Up" : "Down", x, y);
       if(button == 3){
           zoomlevel+= 0.4;
       }else if(button == 4){
           zoomlevel -= 0.4;
       }
   }else{  // botoes normais
       printf("Scroll %s At %d %d\n", (state == GLUT_DOWN) ? "Up" : "Down", x, y);
       if (state == GLUT_DOWN){
           initial_x = x;
       }else if(state == GLUT_UP){
           mouse_x += (x - initial_x)/100;
       }
   }
}

void draw(void)
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
        glTranslatef(mouse_x, mouse_y, zoomlevel);
        glRotatef(angle, 0, 1, 0);
        glRotatef(60, 1, 0, 0);

        LoadItens("sol.jpg");
        gluQuadricDrawStyle(sphere, GLU_FILL);
        glBindTexture(GL_TEXTURE_2D, tex);
        gluQuadricTexture(sphere, GL_TRUE);
        gluQuadricNormals(sphere, GLU_SMOOTH);
        gluSphere(sphere, 5, 32, 16);
        glDeleteTextures( 1, &tex );
    glPopMatrix();

        glPushMatrix();
            glTranslatef(mouse_x+10, mouse_y, zoomlevel);
            glRotatef(angle, 0, 1, 0);
            glRotatef(60, 1, 0, 0);
            LoadItens("merc.jpg");
            gluQuadricDrawStyle(sphere, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, tex);
            gluQuadricTexture(sphere, GL_TRUE);
            gluQuadricNormals(sphere, GLU_SMOOTH);
            gluSphere(sphere, 0.38, 32, 16);
            glDeleteTextures( 1, &tex );
        glPopMatrix();

        glPushMatrix();
            glTranslatef(mouse_x+16, mouse_y, zoomlevel);
            glRotatef(angle, 0, 1, 0);
            glRotatef(60, 1, 0, 0);
            LoadItens("venus.jpg");
            gluQuadricDrawStyle(sphere, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, tex);
            gluQuadricTexture(sphere, GL_TRUE);
            gluQuadricNormals(sphere, GLU_SMOOTH);
            gluSphere(sphere, 0.94, 32, 16);
            glDeleteTextures( 1, &tex );
        glPopMatrix();
            
        glPushMatrix();
            glTranslatef(mouse_x+24, mouse_y, zoomlevel);
            glRotatef(angle, 0, 1, 0);
            glRotatef(270, 1, 0, 0);
            LoadItens("terra2.jpg");
            gluQuadricDrawStyle(sphere, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, tex);
            gluQuadricTexture(sphere, GL_TRUE);
            gluQuadricNormals(sphere, GLU_SMOOTH);
            gluSphere(sphere, 1, 32, 16);
            glDeleteTextures( 1, &tex );

        glPopMatrix();

            glPushMatrix();
                LoadItens("lua.jpg");
                glTranslatef(mouse_x+27, mouse_y, zoomlevel);
                glRotatef(angle, 0, 1, 0);
                gluQuadricDrawStyle(sphere, GLU_FILL);
                glBindTexture(GL_TEXTURE_2D, tex);
                gluQuadricTexture(sphere, GL_TRUE);
                gluQuadricNormals(sphere, GLU_SMOOTH);
                gluSphere(sphere, 0.27, 32, 16);
                glDeleteTextures( 1, &tex );
            glPopMatrix();

        glPushMatrix();
            glTranslatef(mouse_x+35, mouse_y, zoomlevel);
            glRotatef(angle, 0, 1, 0);
            glRotatef(60, 0.53, 0, 0);
            LoadItens("marte.jpg");
            gluQuadricDrawStyle(sphere, GLU_FILL);
            glBindTexture(GL_TEXTURE_2D, tex);
            gluQuadricTexture(sphere, GL_TRUE);
            gluQuadricNormals(sphere, GLU_SMOOTH);
            gluSphere(sphere, 0.53, 32, 16);
            glDeleteTextures( 1, &tex );

            // lua aqui pushmatrix
        glPopMatrix();


    glPopMatrix();

    glFlush();
    glutSwapBuffers();

}

void rotation(int value){
    angle += 1;

    glutTimerFunc(50, rotation, 1);
    glutPostRedisplay();
}

void resize(int w, int h)
{
    if (!h)
	h = 1;
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90.0, 1.0*w/h, 0.1, 100.0);
}

void init(void)
{
    glEnable(GL_DEPTH_TEST);
    //make_tex();
    sphere = gluNewQuadric();
    glEnable(GL_TEXTURE_2D);
    // loaditens is leaky
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(640, 320);

    glutCreateWindow("Test");

    glutDisplayFunc(draw);
    glutReshapeFunc(resize);
    glutTimerFunc(50, rotation, 1);
    glutMouseFunc(mouse);

    init();

    glutMainLoop();
}
