#include <stdlib.h> 
#include <GL/glut.h> 
#include <stdio.h> 
#include <SOIL/SOIL.h>
  
#define INIT_VIEW_X  0.0
#define INIT_VIEW_Y  0.0
#define INIT_VIEW_Z  -12.5

#define VIEW_LEFT     -2.0 
#define VIEW_RIGHT     2.0 
#define VIEW_BOTTOM   -2.0 
#define VIEW_TOP       2.0 
#define VIEW_NEAR      1.0 
#define VIEW_FAR     200.0 
GLuint tex;
float sunrev = 0.0;        // revoluções do sol
float mercrev = 0.0;
float mercorb = 0.0;
float venusrev = 360.0;
float venusorb = 0.0;
float terraorb = 0.0;
float terrarev = 0.0;
float luarev = 0.0;
float luaorb = 0.0;
float marterev = 0.0;
float marteorb = 0.0;
float zoomlevel = 0.0;


int fullscreen = 0;               
int initial_x;
int initial_y;
int drag = 0;
int xpos = 0;
int ypos = 0;
  
GLfloat AmbientLight[]  = {0.3f, 0.3f, 0.3f, 1.0f}; 
GLfloat DiffuseLight[]  = {0.8f, 0.8f, 0.8f, 1.0f}; 
GLfloat SpecularLight[] = {1.0f, 1.0f, 1.0f, 1.0f}; 
GLfloat SpecRef[]       = {0.7f, 0.7f, 0.7f, 1.0f}; 
GLfloat LightPos[]      = {-50.0f,50.0f,100.0f,1.0f}; 
GLubyte Shine           = 128; 
  
void SetupRend() 
{ 
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 
    glEnable(GL_DEPTH_TEST); 
    glEnable(GL_LIGHTING); 
    glLightfv(GL_LIGHT0, GL_AMBIENT, AmbientLight); 
    glLightfv(GL_LIGHT0, GL_DIFFUSE, DiffuseLight); 
    glLightfv(GL_LIGHT0, GL_SPECULAR, SpecularLight); 
    glEnable(GL_LIGHT0); 
    glEnable(GL_COLOR_MATERIAL); 
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE); 
    glMaterialfv(GL_FRONT, GL_SPECULAR, SpecRef); 
    glMateriali(GL_FRONT, GL_SHININESS, Shine); 
}  
  
void mouse(int button, int state, int x, int y)
{
   // 3 scroll up, 4 scroll down
   if ((button == 3) || (button == 4))
   {
       if (state == GLUT_UP) return; // Disregard redundant GLUT_UP events
       printf("Scroll %s At %d %d\n", (button == 3) ? "Up" : "Down", x, y);
       printf("%d\n",zoomlevel);
       if(button == 3){
           zoomlevel += 0.4;
       }else if(button == 4){
           zoomlevel -= 0.4;
       }
   }else{  // botoes normais
       printf("Scroll %s At %d %d\n", (state == GLUT_DOWN) ? "Up" : "Down", x, y);
       if (state == GLUT_DOWN){
           drag = 1;
       }else{
           drag = 0;
       }
   }
}

void Dragger(int x, int y){
    if(drag == 1){
        if (x < initial_x){
            xpos -= x/100;
            initial_x = xpos;
        }else if(xpos > initial_x){
            xpos += x/100;
            initial_x = xpos;
        }
        if (y < initial_y){
            ypos -= y/100;
            initial_y = ypos;
        }else if(y > initial_y){
            ypos += y/100;
            initial_y = ypos;
        }
         glutPostRedisplay();
    }
}


static void LoadItens(char* filename){
        tex = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y );
        if( 0 == tex )
        {
            printf( "erro do SOIL: '%s'\n", SOIL_last_result() );
        }

        // seta os parametros para a textura
        glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_DECAL);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // habilita a textura 2d para ser inserida nos objetos
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, tex);

}


void Display(void) 
{ 
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
  
    glPushMatrix(); 
         glRotatef (ypos,1.0,0.0,0.0);
        glTranslatef(0.0,0.0,zoomlevel);        // sets the radius of the orbit 
         glRotatef (90.0,1.0,0.0,0.0);
        glRotatef(sunrev,0.0,0.0,1.0); 



        GLUquadricObj* sol = gluNewQuadric();
        LoadItens("sol.jpg");
        gluQuadricDrawStyle(sol,GLU_FILL);
        gluQuadricTexture(sol,GL_TRUE);
        glBindTexture(GL_TEXTURE_2D,tex);
        gluSphere(sol,1.0,20,20);

    // ...

        gluDeleteQuadric(sol);
        glDeleteTextures( 1, &tex );

        //mercurio
    glPushMatrix();
        glRotatef(mercorb,0.0,0.0,1.0); // orbits the planet around the sun   
        glTranslatef(1.5,0.0,0.0);        // sets the radius of the orbit 
        glRotatef(mercrev,0.0,0.0,1.0); // revolves the planet on its axis 
        GLUquadricObj* merc = gluNewQuadric();
        LoadItens("merc.jpg");
        gluQuadricDrawStyle(merc,GLU_FILL);
        gluQuadricTexture(merc,GL_TRUE);
        glBindTexture(GL_TEXTURE_2D,tex);
        gluSphere(merc,0.1,20,20);
        gluDeleteQuadric(merc);
        glDeleteTextures( 1, &tex );
    glPopMatrix();
        // venus
    glPushMatrix();
            glRotatef(venusorb,0.0,0.0,1.0); // orbits the planet around the sun   
            glTranslatef(2.5,0.0,0.0);        // sets the radius of the orbit 
            glRotatef(venusrev,0.0,0.0,1.0); // revolves the planet on its axis 
            GLUquadricObj* venus = gluNewQuadric();
            LoadItens("venus.jpg");
            gluQuadricDrawStyle(venus,GLU_FILL);
            gluQuadricTexture(venus,GL_TRUE);
            glBindTexture(GL_TEXTURE_2D,tex);
            gluSphere(venus,0.5,20,20);
            gluDeleteQuadric(venus);
            glDeleteTextures( 1, &tex );
    glPopMatrix();
        // terra
    glPushMatrix();
            glRotatef(terraorb,0.0,0.0,1.0); // orbits the planet around the sun   
            glTranslatef(5.5,0.0,0.0);        // sets the radius of the orbit 
            glRotatef(terrarev,0.0,0.0,1.0); // revolves the planet on its axis 
            GLUquadricObj* terra = gluNewQuadric();
            LoadItens("terra3.jpg");
            gluQuadricDrawStyle(terra,GLU_FILL);
            gluQuadricTexture(terra,GL_TRUE);
            glBindTexture(GL_TEXTURE_2D,tex);
            gluSphere(terra,0.6,20,20);
            gluDeleteQuadric(terra);
            glDeleteTextures( 1, &tex );

            //lua
            glRotatef(luaorb,0.0,0.0,1.0); // orbits the planet around the sun   
            glTranslatef(0,1,0.0);        // sets the radius of the orbit 
            glRotatef(luarev,0.0,0.0,1.0); // revolves the planet on its axis 
            GLUquadricObj* lua = gluNewQuadric();
            LoadItens("lua.jpg");
            gluQuadricDrawStyle(lua,GLU_FILL);
            gluQuadricTexture(lua,GL_TRUE);
            glBindTexture(GL_TEXTURE_2D,tex);
            gluSphere(lua,0.2,20,20);
            gluDeleteQuadric(lua);
            glDeleteTextures( 1, &tex );
    glPopMatrix();
    //marte
    glPushMatrix();
            glRotatef(marteorb,0.0,0.0,1.0); // orbits the planet around the sun   
            glTranslatef(8.5,0.0,0.0);        // sets the radius of the orbit 
            glRotatef(marterev,0.0,0.0,1.0); // revolves the planet on its axis 
            GLUquadricObj* marte = gluNewQuadric();
            LoadItens("marte.jpg");
            gluQuadricDrawStyle(marte,GLU_FILL);
            gluQuadricTexture(marte,GL_TRUE);
            glBindTexture(GL_TEXTURE_2D,tex);
            gluSphere(marte,0.3,20,20);
            gluDeleteQuadric(marte);
            glDeleteTextures( 1, &tex );
    glPopMatrix();
    glPopMatrix(); 
  
    glutSwapBuffers(); 
} 
  
void ChangeWindow(GLsizei w, GLsizei h)   
{ 
    GLfloat Ratio; 
    if (h==0) 
        h=1; 
    glViewport(0,0,w,h); 
    Ratio = (GLfloat)w/(GLfloat)h; 
    glMatrixMode( GL_PROJECTION ); 
    glLoadIdentity(); 
    gluPerspective(50.0f, Ratio, VIEW_NEAR, VIEW_FAR); 
    glMatrixMode( GL_MODELVIEW ); 
    glLoadIdentity(); 
    glTranslatef( INIT_VIEW_X, INIT_VIEW_Y, INIT_VIEW_Z ); 
    glLightfv(GL_LIGHT0, GL_POSITION, LightPos); 
} 
  
void CheckRot() 
{ 
    if(sunrev>360.0) 
        sunrev-=360.0; 
    if(sunrev<0.0) 
        sunrev+=360.0; 

    if(mercrev>360.0) 
        mercrev-=360.0; 
    if(mercrev<0.0) 
        mercrev+=360.0; 
    if(mercorb>360.0) 
        mercorb-=360.0; 
    if(mercorb<0.0) 
        mercorb+=360.0; 

    if(venusrev>360.0) 
        venusrev-=360.0; 
    if(venusrev<0.0) 
        venusrev+=360.0; 
    if(venusorb>360.0) 
        venusorb-=360.0; 
    if(venusorb<0.0) 
        venusorb+=360.0; 

    if(terrarev>360.0) 
        terrarev-=360.0; 
    if(terrarev<0.0) 
        terrarev+=360.0; 
    if(terraorb>360.0) 
        terraorb-=360.0; 
    if(terraorb<0.0) 
        terraorb+=360.0; 

    if(luarev>360.0) 
        luarev-=360.0; 
    if(luarev<0.0) 
        luarev+=360.0; 
    if(luaorb>360.0) 
        luaorb-=360.0; 
    if(luaorb<0.0) 
        luaorb+=360.0; 

    if(marterev>360.0) 
        marterev-=360.0; 
    if(marterev<0.0) 
        marterev+=360.0; 
    if(marteorb>360.0) 
        marteorb-=360.0; 
    if(marteorb<0.0) 
        marteorb+=360.0; 
} 
  
void SpecialKeys(int key, int x, int y) 
{ 
    // will be used to change rotation angles and rates
    if (key == GLUT_KEY_LEFT) 
    { 
  
    } 
   
    if (key == GLUT_KEY_RIGHT) 
    { 
  
    } 
        
    if (key == GLUT_KEY_UP) 
    { 
  
    } 
  
  
    if (key == GLUT_KEY_DOWN) 
    { 
  
    } 
  
    CheckRot(); 
    glutPostRedisplay(); 
} 
  
void KeyPress (unsigned char key, int x, int y) 
{ 
    switch (key) 
    {       
        case 'f': 
             if (fullscreen==1) 
             { 
                 glutReshapeWindow(320,240); 
                 glutPositionWindow(5,30); 
             } 
             else 
                glutFullScreen(); 
            fullscreen = 1-fullscreen; 
            break; 
  
        case 'F': 
             if (fullscreen==1) 
             { 
                 glutReshapeWindow(320,240); 
                 glutPositionWindow(5,30); 
             } 
             else 
                glutFullScreen(); 
            fullscreen = 1-fullscreen; 
            break; 
       
        case 'Q': 
            exit(1); 
            break; 
       
        case 'q': 
            exit(1); 
            break;   
    } 
    glutPostRedisplay(); 
} 
  
void animate(void) 
{ 
    sunrev +=1;
    mercrev+=0.017;
    mercorb+=4.34;
    venusrev-=0.66;
    venusorb+=1.63;
    terraorb+=1;
    terrarev+=1;
	luarev +=0.03;
	luaorb +=14;
    marterev +=1;
    marteorb += 0.5;
    CheckRot();
  
    glutPostRedisplay(); 
} 
  
int main(int argc, char *argv[])
{ 
    glutInit( & argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);  // double buffered window 
    glutCreateWindow ("Orbit Demo");                        // Graphics Window Title 
    glutReshapeWindow(640,480);                             // Graphics Window Size 
    SetupRend();                                            // lighting and material properties 
    glutReshapeFunc (ChangeWindow);                         // function to maintain aspect ratios 
    glutKeyboardFunc (KeyPress);                            // function to handle key press 
    glutSpecialFunc (SpecialKeys);                          // function to handle special keys 
    glutMouseFunc(mouse);
    glutMotionFunc(Dragger);
    glutDisplayFunc (Display);                              // graphics display function 
    glutIdleFunc(animate);                                  // updates object motions 
    glutMainLoop();                                         // enters the GLUT event processing loop 
}
