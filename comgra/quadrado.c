#include <stdio.h>

int main(){
    int n=0;

    while(n < 2){
        printf("Digite a altura do quadrado.\n");
        printf("*Deve ser  maior que 2\n");
        scanf("%d", &n);
        if(n < 2) {
            printf("Valor invalido\n");
        }
    }

    int i;
    int j;
    int lin = n;
    int col = n;
    for(i=0;i<lin;i++){
        for(j=0;j<col;j++){
            // Se for a primeira ou ultima linha, imprimir * em cada coluna
            if(i == 0 || i == lin-1){
                printf("*");
            }
            else{
                // se primeira , ultima coluna ou linha = coluna (diagonal)
                if(j == 0 || j == col-1 || i == j){
                    printf("*");
                }
                else{
                    printf(" ");
                }
            }
        }
            printf("\n");
    }
}
