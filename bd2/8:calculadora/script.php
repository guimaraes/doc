<?php

$op = $_POST['op'];
$num1 = $_POST['num1'];
$num2 = $_POST['num2'];

switch ($op) {
case '1':
	echo $num1. " + ". $num2." = ". ($num1 + $num2);
	break;
case '2':
	echo $num1. " - ". $num2. " = ". ($num1 - $num2);
	break;
case '3':
	echo $num1. " * ". $num2. " = ". ($num1 * $num2);
	break;
case '4':
	if ($num2==0) {
		echo "Impossível dividir por zero";
	} else {
		echo $num1. " / ". $num2. " = ". ($num1 / $num2);
	}
	break;
}
