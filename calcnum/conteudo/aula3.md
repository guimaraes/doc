# Cálculo númerico

## Método de iteração de ponto fixo
### Definição
O ponto $p$ é ponto fixo da função $g$, se $g(p) = p$
> Ponto de interseção do gráfico da função $g$ e a reta $y=x$

Por exemplo, Se $g(x) = x^2 -2 $, a função admite os pontos fixos $p=-1$ e $p=2$, pois:
- $g(-1) = 1^2 - 2 = -1$
- $g(2) = 2^2 - 2 = 2$


Em forma direta:

$g(p) = p$

$p^2-2=p$

$p^2 - p - 2 = 0$

$ (p+1) * (p-2) = 0$

- $ p1 = -1$
- $p2 = 2$

### Condições suficientes para a existência de um ponto fixo

Teorema:
1.  Se $g([a,b]) C [a,b]$, então $g$ admite pelo menos um ponto fixo em $[a,b]$.
2.  Se $g \in l^1 (a,b)$ e existe $0 < k < 1 $, e $ | g'(x) | \leq k, \forall x \in (a,b)$ então $g$ admite um único ponto fixo em $ [ a,b] $.

#### Prova
##### i )
- Se $g(a) = a$ ou $g(b) = b$ então, a ou b são pontos fixos.

- Se $a<g(x)<b$, em particular $a<g(a)$ e $g(b) <b$  $\rightarrow$ $g(a)-a >0$ e $g(b) -b <0$.

- Seja $h(x) = g(x) -x, h \in l[a,b]$ e $h(a) >0$ e $h(b) < 0$

- Pelo teorema do valor intermediário existe $p \in (a,b)$ tal que:

 - $h(p) = 0$

 - $g(p) -p =0$

 -  $g(p) = p$

- $p$ é ponto fixo de $g$.

 ##### ii )
 Vamos supor que existam dois pontos fixos: $p$ e $q$, $p \neq q$

 $\Downarrow$

 $g(p) = p$

 $g(q) = q$

 $ \frac {g(p) - g(q)}{p-q} = g'(E)$ , $E \in (a,b)$, **teorema do valor intermediário**

 $\Downarrow$

 $|\frac{ p-q}{p-q}| = | g'(E)|$

 $\Downarrow$

 - Contradição, pois $|g'(E)| \leq k < 1$

Portanto $p=q$ e um único ponto fixo.

### Ponto Fixo e Zero de uma função

#### a):
- Dado o problema de calcular $p$ tal que $f(p)=0$, pode-se definir funções $g$ tal que $p$ é ponto fixo de $g$

##### Exemplo
$g(x) = x-f(x)$

$g(p) = p -$ ~~$f(p)$~~

$g(p) = p$

$p$ é ponto fixo de $g(x) = x-f(x)$

ou :
- $g(x) = x+3*f(x)$

- $g(p) = p +3 f(p)$ (0)

- $g(p) = p$

$p$ é ponto fixo de $g(x) = x + 3*f(x)$

> O ideal é construir $g$ que verifique as condições do teorema

#### b ):
- Reciprocamente, se $p$ é ponto fixo de $g$ isto é, $p(p) = p$, então $p$ é zero da função $f(x) = x-g(x)$, pois: $f(p)= p-g(p) = p-p = 0$

##### Exemplo
Mostre que a função $g(x) = \frac{x^2-1}{3}$ tem um único ponto fixo em $[-1,1]$

###### Solução:
Os pontos máximos e mínimos em $[1,-1]$ podem ocorrer em $x=-1$,$x=1$ quando $g'(x)=0$.

| x    | $\mathbf{g(x) = \frac{x^2-1}{3}}$|
| ---- | -------------- |
| -1   | 0              |
| 0    | $-\frac{1}{3}$ |
| 1    | 0              |

$\Downarrow$

**Existe pelo menos um ponto fixo de $\mathbf{g}$ em $\mathbf{[-1,1]}$.**

$| g'(x) | = |\frac{2}{3} * x | = \frac{2}{3} * |x| \leq \frac{2}{3} < 1$

$\Downarrow$

$g$ admite um único ponto fixo em $[-1,1]$



##### Cálculo dos pontos fixos:
$g(p) = p$

$\frac{p^2-1}{3}=p$

$\Downarrow$

$p^2 - 3*p-1=0$

$p = \frac{3 +- \sqrt 9+4}{2} = \frac{3+- \sqrt 3}{2}$

$p1 = \frac{3- \sqrt 3}{2} \in [-1,1]$

$p1 = \frac{3+ \sqrt 3}{2} \notin [-1,1]$

Porém:

- $p2 \in [3,3]$
- $g(3) = \frac{3^2-1}{3} = \frac{8}{3} \notin [3,4]$
- $g(4) = \frac{4^2-1}{3} = 5 \notin [3,4] $
- $g([3,4]) \not\subset [3,4]$
- $|g(x)| = |\frac{2}{3} * x| < \frac{2-4}{3} = \frac{8}{3}$

Não se verificam as condições do teorema, entretanto existe um único ponto fixo em $[3,4]$


>As condições do teorema são suficientes, mas não necessárias para a existência de um ponto fixo de uma função.

### Iteração de ponto fixo

Para aproximar o ponto fixo de uma função $g$ em $[a,b]$

#### a):
- Escolhemos uma aproximação inicial $p0 \in [a,b]$

#### b):
- Geramos uma sequência $ {Pn}^{\infty}_{n=0}$ , onde $Pn = g(Pn-1)$, $n \geq 1$

Se $g$ é contínua em $[a,b]$ e $Pn \rightarrow p$, então :

$ p = \displaystyle\lim_{n \to \infty} Pn = \displaystyle\lim_{n \to \infty} g(Pn-1)$

$= g (\lim Pn \neg)$, pois g é contínua

$\Downarrow$

$p = g(p)$

$\Downarrow$

$p = lim Pn$ é ponto fixo da função $g$ $n \rightarrow \infty$


![img](/home/kassio/Documents/calcnum/img/aula3_graf1.jpg)



### Algoritmo: Iteração de ponto fixo

#### Objetivo :

Determinar a solução $p=g(p)$ dada uma aproximação inicial $p0$ 



##### Entrada:

- aproximação inicial $p0$
- tolerância: $tol$
- máximo de iterações : $MAXIT$

##### Saída: 

- Solução aproximada ou mensagem de falha

##### Passos:

> 1. $ i \leftarrow 1$ 
>
> 2. Enquanto $i<MAXIT$ fazer passos 3 ao 6
>
> 3. $p \leftarrow g(p0)$
>
> 4. Se $ | p - p0 | < tol $ então:
>
>    saída:  $p$
>
>    stop
>
> 5. $ i \leftarrow i+1$
>
> 6. $ p0 \leftarrow p$
>
> 7. saída: "O método falhou após $MAXIT$ iterações
>
>    stop



#### Tarefa

 Implementar numa linguagem qualquer o algoritmo acima.



### Teorema do ponto fixo



Seja $g \in \complement[a,b]$, e:

- a) $g([a,b])$ $\subset [a,b]$
- b) $g'$ existe em $(a,b)$ e existe uma constante $k$, $0<k<1$, tal que $ |g'(x)| < k, \forall x \in (a,b)$ .

Então, para qualquer $p0 \in [a,b]$ a sequência $Pn = g (Pn-1)$ converge ao único ponto fixo $p$ em $[a,b]$ .



#### Prova

![prova1](/home/kassio/Documents/calcnum/img/aula3_prova1.jpg)




#### Corolário:

1. Se $g$ verifica os limites do teorema, então as cotas para o erro envolvido usando $Pn$ para aproximar $Pn$ são:

- $| Pn - p| \leq K^n *  max (p0 - a, b-p0)$


- $ | Pn - p| \leq  \frac{K^n}{1-k} * | p1-p0 |$ , $\forall n \geq 1$ 

> A taxa de convergência depente do fator $K^n$

2. Quanto menor o valor de $k$ mais rápida é a convergência


3.  A convergência pode ser muito lenta se $k$ é próximo de $1$



#### Exemplo

Usando o algoritmo da iteração de ponto fixo, determine os zeros da função $ f(x) = x^3 + 4x^2-10$, $x \in [1,2]$ 

> $g = ?$ 
>
> pontos fixos de g serão os zeros de f
>
> 

 




