def lu_fat(A):
    n = len(A)

    L = []
    C = [0] * n
    L = [C] * n
    U = L
    print(A)
    print(L)
    print(U)

    # Passo 1
    L[0][0] = A[0][0]
    U[0][1] = A[0][1] / L[0][0]

    for i in range(1, n-1):
        L[i][i-1] = A[i][i-1]
        L[i][i] = A[i][i] - L[i][i-1] * U[i][i-1]
        U[i][i+1] = A[i][i+1] / L[i][i]
        C[i] = (A[i][n] - L[i][i-1] * C[i-1]) / L[i][i]

    # Passo 3
    L[n-1][n-2] = A[n-1][n-1]
    L[n-1][n-1] = A[n-1][n-1] - L[n-1][n-2] * U[n-2][n-1]
    C[n-1] = (A[n-1][n] - L[n-1][n-1] * C[n-2]) / L[n-2][n-2]

    # Passo 4
    X = [0] * n
    print(C)
    X[n] = C[n]

    # Passo 5
    for i in range(n-1, 0, -1):
        X[i] = C[i] - U[i][i+1] * X[i+1]

    print(X)


entrada = []
entrada.append([2, -1, 0, 0, 1])
entrada.append([-1, 2, -1, 0, 0])
entrada.append([0, -1, 2, -1, 0 ])
entrada.append([0, 0, -1, 2, 1])

lu_fat(entrada)
