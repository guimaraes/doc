import math
def f(x):
    # f(x) =  x^3-x-1
    #res = g(x) -> f(x) = 0
    res = pow(x+1,1/3)
    return res

def itpf(x, tol=0.0001, maxit=50):
    i=1
    while i<maxit:
        res = f(x)
        if (abs(res - x)) < tol:
            print ("Resolvido na iteração " + str(i) + " com tolerância " + str(tol))
            print ("Ultima ent: " + str(x))
            print ("Ultimo res: " + str(res))
            return res


        i = i+1
        x = res
    print("O método falhou após: " + str(maxit) + " iterações" )



itpf(1)

