def func(x):
    result = 4 * (x ** 3)
    return result


def simpson(up_lim, low_lim, n, func):
    step = up_lim / n
    sum = 0
    count = 1
    x = low_lim + step

    # pares * 2 e impares * 4
    while x <= up_lim - step:
        if count % 2 == 0:
            sum += 2 * func(x)
        else:
            sum += 4 * func(x)

        count += 1
        x += step

    # extremos
    sum += func(low_lim)
    sum += func(up_lim)

    h = (up_lim - low_lim) / n
    sum = sum * h/3

    print(sum)
    return(sum)


x = simpson(2, 0, 4, func)
