#Interpolação por diferenças divididas
class Ponto:
    def __init__(self,x,y):
        self.x = x
        self.y = y

def difdiv(pontos, valor):
    #passo 1
    for i in range(0,len(pontos),1):
        F[i,0] = pontos[i].y
        
    #passo 2
    for i in range(1,len(pontos)):
        for j in range(1,i+1,1):
            F[i,j] = ( ( F[i, j-1] - F[i-1, j-1] ) / (pontos[i].x - pontos[i-j].x) )

    #passo 3
    soma = F[0,0]
    for i in range( 1, len(pontos) ):
        prod = 1
        for j in range(0,i):
            prod = prod * (valor - pontos[j].x)
        soma = soma + (F[i,i] * prod)
    print(soma)
    return (soma)
#fim função

F = {}
p1 = Ponto(1, 0.7651977)
p2 = Ponto(1.3, 0.6200860)
p3 = Ponto(1.6, 0.4554022)
p4 = Ponto(1.9, 0.2818186)
p5 = Ponto(2.2, 0.1103623)

pontos = [p1, p2, p3, p4, p5]
difdiv(pontos,1)