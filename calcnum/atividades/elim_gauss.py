def elim_gauss(A):
    num_elementos = len(A)  # quantidade de elementos por linha
    # o vetor resultado tem tamanho igual a quantidade de linhas da matriz
    resultado = [0] * num_elementos
    # Passo 1
    for i in range(0, num_elementos-1):
        # Passo 2
        pivo = i
        for j in range(i, num_elementos):
            if(A[pivo][i] == 0):
                pivo = pivo + 1

        if(pivo == num_elementos):
            print('Não existe solução única')
        else:
            # Passo 3
            if(pivo != i):
                A[pivo], A[i] = A[i], A[pivo]

            # Passo 4
            for j in range(i+1, num_elementos):
                # Passo 5
                m = A[j][i]/A[i][i]
                # Passo 6
                A[j] = list(map(lambda x, y: x - m * y, A[j], A[i]))

                # O objetivo é atualizar a linha atual: Lj <- Lj - m * Li
                # list() sinaliza que A[j] receberá uma lista
                # lambda é uma função única no que retorna  x - m*y
                # map aplica a lambda aos elementos de Aj e de Ai como x e y
                # Utilizando for:
                # for f in (range 0, num_elementos):
                #   A[j][f] = A[j][f] - m * A[i][f]

    # Passo 7
    # ref é o número de elementos - 1
    ref = num_elementos - 1

    if(A[ref][ref] == 0):
        print('Não existe solução única')
    else:
        # Passo 8
        resultado[ref] = A[ref][num_elementos]/A[ref][ref]

        # Passo 9
        for i in range(ref - 1, -1, -1):
            soma = A[i][num_elementos]
            for j in range(i+1, num_elementos):
                soma = soma-A[i][j] * resultado[j]
                resultado[i] = (soma/A[i][i])
    # Passo 10
    return(resultado)


A = []
A.append([0, 1, 0, 3, 4])
A.append([2, 1, -1, 1, 1])
A.append([3, -1, -1, 2, -3])
A.append([-1, 2, 3, -1, 4])

print(elim_gauss(A))
