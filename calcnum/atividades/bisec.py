#!/usr/bin/python3
import math
from prettytable import PrettyTable
def func(x):
    t = pow(x,3) + 4*(pow(x,2)) -10
    return  t
    
def bisec(a,b):
    maxIT = 50
    tol = 0.00001
    it = 0
    t = PrettyTable(['k','ak','bk','pk','f(pk)'])
    while it<maxIT:
        ar = []
        c = (b-a)/2
        p = a + (b-a)/2
        if c<tol or abs(func(p))<tol:
            print (t)
            return p
        ar.append(it)
        ar.append(a)
        ar.append(b)
        ar.append(p)
        ar.append(func(p))
        t.add_row(ar)
        if (func(a)*func(p)) > 0:
            a=p
        else:
            b=p

        it=it+1
    print ("O método falhou após:" + str(maxIT) + " iterações")

bisec(1,2)
