<TeXmacs|1.99.5>

<style|<tuple|generic|portuguese>>

<\body>
  <\with|font|TeX Gyre Schola|font-base-size|10>
    <doc-data|<doc-title|F�sica 2: Lista 1>|<doc-author|<\author-data|<author-name|K�ssio
    Guimaraes>>
      \;
    </author-data>>>

    \;

    \;

    1. Calcule a dist�ncia entre dois pr�tons para que o m�dulo da for�a
    el�trica

    repulsiva entre os pr�tons seja igual ao peso de um pr�ton na superf�cie
    da terrestre. Sendo

    a massa do pr�ton igual a 1,67x10<math|<rsup|-27>> Kg e a carga elementar
    e = 1,6x10<math|<rsup|-19>>C.

    <with|font|TeX Gyre Schola|font-base-size|9|<\indent>
      <strong|Solu��o:>

      1. Consideramos a carga elementar como
      <math|1.6\<times\>10<rsup|-19>C******>

      2. A for�a entre duas cargas pode ser calculada pela lei de Coulomb:
      <math|k\<times\><frac|Q1\<times\>Q2|d<rsup|2>>>

      <\compact>
        <\framed>
          Onde:

          <math|k> � a constante de Coulomb, que representa o meio

          <math|Q1> e <math|Q2> representam as duas cargas

          <math|d> representa a dist�ncia entre as duas cargas
        </framed>
      </compact>

      3. A massa de um p�oton � <math|1.67\<times\>10<rsup|-27>> kg, logo seu
      peso � <math|1.67\<times\>10<rsup|-27>\<times\>9.8> =
      <math|16.366\<times\>10<rsup|-27>>

      \;

      A for�a de atra��o entre as duas cargas se d� por
      <math|k*\<times\><frac|<around*|(|1.6*\<ast\>10<rsup|-19>|)>\<times\><around*|(|1.6\<times\>10<rsup|-19>|)>|d<rsup|2>>>

      O problema pede a dist�ncia necess�ria para que a for�a el�trica
      repulsiva seja igual ao peso de um pr�ton, logo temos:

      <\center>
        <math|16.366\<ast\>10<rsup|-27><rsup|>=k*\<times\><frac|<around*|(|1.6*\<times\>10<rsup|-19>|)>\<times\><around*|(|1.6\<times\>10<rsup|-19>|)>|d<rsup|2>>><center|>

        \;
      </center>

      Considerando que a intera��o ocorre no v�cuo, temos que
      <math|k=9\<times\>10<rsup|9>>

      <center|<math|16.366\<times\>10<rsup|-27><rsup|>=9\<times\>10<rsup|9>*\<times\><frac|2.56<rsup|-38>|d<rsup|2>>>>

      Logo d= 0.11865 (11.8 centimetros)

      <\indent>
        \;
      </indent>
    </indent>>

    \;

    \;

    2. A carga total de duas pequenas esferas positivamente carregadas vale
    5x10<math|<rsup|-5>> C.

    Determine a carga total de cada esfera, sabendo que quando a dist�ncia
    entre as esferas �

    2m, a for�a de repuls�o possui m�dulo igual a 0,9N.

    <\indent>
      <\strong>
        Solu��o:
      </strong>
    </indent>

    <\indent>
      1. A carga somada das duas esferas positivamente carregadas � de
      <math|5\<times\>10<rsup|-5>C>

      2. A for�a de repuls�o entre elas � de <math|0.9N>

      3. As cargas pode ser determinadas pela lei de Coulomb:
      <math|k\<times\><frac|Q1\<times\>Q2|d<rsup|2>>>

      <\compact>
        <\framed>
          Onde:

          <math|k> � a constante de Coulomb, que representa o meio

          <math|Q1> e <math|Q2> representam as duas cargas

          <math|d> representa a dist�ncia entre as duas cargas
        </framed>
      </compact>

      \;

      Podemos aplicar a lei de Coulomb para determinar a multiplica��o das
      cargas:

      <math|0.9=k\<times\><frac|Q1\<times\>Q2|2<rsup|2>>>

      Considerando que a intera��o ocorre no v�cuo, temos que
      <math|k=9\<times\>10<rsup|9>>, logo

      <math|0.9=9\<times\>10<rsup|9>\<times\><frac|Q1\<times\>Q2|2<rsup|2>>>

      <math|0.9\<times\>4=9\<times\>10<rsup|9*>\<times\>Q1\<times\>Q2>

      <math|<frac|3.6|9\<times\>10<rsup|9>>=Q1\<times\>Q2>

      <math|Q1\<times\>Q2=4\<times\>10<rsup|-10>>

      Como a multiplica��o entre os dois � <math|5\<times\>10<rsup|-5>> e a
      soma <math|4\<times\>10<rsup|-10>> podemos deduzir que um resultado
      poss�vel seria :

      <\math>
        Q1=1\<times\>10<rsup|-5>

        Q2=4\<times\>10<rsup|-5>
      </math>

      <\indent>
        \;
      </indent>
    </indent>

    \;

    3. Em cada v�rtice de um tri�ngulo equil�tero de lado igual a L, existe
    uma carga

    q. Determine o m�dulo da for�a que atua sobre qualquer uma das tr�s
    cargas em fun��o de

    L e de q.
  </with>
</body>

<\initial>
  <\collection>
    <associate|font|schola>
    <associate|font-base-size|11>
    <associate|font-family|rm>
    <associate|math-font|math-schola>
    <associate|page-medium|paper>
  </collection>
</initial>